package khasanah.isna.musiclibrary

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

import kotlinx.android.synthetic.main.activity_genre.*


class AdapterDataGenre(val dataGenre: List<HashMap<String,String>>,
                       val genreActivity: GenreActivity
) : //new
    RecyclerView.Adapter<AdapterDataGenre.HolderDataGenre>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataGenre {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data_genre,p0,false)
        return HolderDataGenre(v)
    }

    override fun getItemCount(): Int {
        return dataGenre.size
    }

    override fun onBindViewHolder(p0: HolderDataGenre, p1: Int) {
        val data = dataGenre.get(p1)
        p0.txIdGenre.setText(data.get("id_genre"))
        p0.txNamaGenre.setText(data.get("nama_genre"))

        //beginNew
        if(p1.rem(2) == 0) p0.bLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.bLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.bLayout.setOnClickListener(View.OnClickListener {
            genreActivity.edIdGenre.setText(data.get("id_genre"))
            genreActivity.edNamaGenre.setText(data.get("nama_genre"))
        })
        //endNew
    }

    class HolderDataGenre(v: View) : RecyclerView.ViewHolder(v){
        val txIdGenre = v.findViewById<TextView>(R.id.txIdGenre)
        val txNamaGenre = v.findViewById<TextView>(R.id.txNamaGenre)
        val bLayout = v.findViewById<ConstraintLayout>(R.id.bLayout) //new
    }
}