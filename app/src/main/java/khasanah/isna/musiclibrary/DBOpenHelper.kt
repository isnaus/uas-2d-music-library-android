package khasanah.isna.musiclibrary

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "media"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tLagu= "create table lagu(id_lagu text primary key, judul text not null, artis text not null, gambar text not null)"
        val insLagu= "insert into lagu(id_lagu,judul,artis,gambar) values " +
                "('0x7f0e0002','Itte','Yorushika','0x7f0e0000')," +
                "('0x7f0e0003','Lemon','Kenshi Yonezu','0x7f0e0001')"
        db?.execSQL(tLagu)
        db?.execSQL(insLagu)
    }
//    .field public static final dc_1:I = 0x7f0e0000
//
//    .field public static final dc_2:I = 0x7f0e0001
//
//    .field public static final ms_1:I = 0x7f0e0002
//
//    .field public static final ms_2:I = 0x7f0e0003
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}