package khasanah.isna.musiclibrary

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import khasanah.isna.musiclibrary.AdapterDataGenre
import khasanah.isna.musiclibrary.R
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_genre.*
import org.json.JSONArray
import org.json.JSONObject


class GenreActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var genreAdapter : AdapterDataGenre
    var daftarGenre = mutableListOf<HashMap<String,String>>()
    var url4 = "http://192.168.43.13/musiclibrary/show_data_genre.php"
    var url5 = "http://192.168.43.13/musiclibrary/query_upd_del_ins_genre.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genre)
        genreAdapter = AdapterDataGenre(daftarGenre,this)
        lvGenre.layoutManager = LinearLayoutManager(this)
        lvGenre.adapter = genreAdapter
        btnInsertGenre.setOnClickListener(this)
        btnUpdateGenre.setOnClickListener(this)
        btnDeleteGenre.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataGenre()
    }

    fun queryInsertUpdateDeleteGenre(mode : String){
        val request = object : StringRequest(
            Method.POST,url5,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataGenre()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_genre",edIdGenre.text.toString())
                        hm.put("nama_genre",edNamaGenre.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_genre",edIdGenre.text.toString())
                        hm.put("nama_genre",edNamaGenre.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_genre",edIdGenre.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataGenre(){
        val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarGenre.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var genre = HashMap<String,String>()
                    genre.put("id_genre",jsonObject.getString("id_genre"))
                    genre.put("nama_genre",jsonObject.getString("nama_genre"))
                    daftarGenre.add(genre)
                }
                genreAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertGenre ->{
                queryInsertUpdateDeleteGenre("insert")
            }
            R.id.btnUpdateGenre ->{
                queryInsertUpdateDeleteGenre("update")
            }
            R.id.btnDeleteGenre ->{
                queryInsertUpdateDeleteGenre("delete")
            }
        }
    }

}