package khasanah.isna.musiclibrary

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.view.View
import kotlinx.android.synthetic.main.manajement_activity.*

class ManajementActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.manajement_activity)

        btnManLagu.setOnClickListener(this)
        btnManGenre.setOnClickListener(this)
        btnManKategori.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnManLagu ->{
                var intent = Intent(this,LaguActivity::class.java)
                startActivity(intent)
            }
            R.id.btnManGenre ->{
                var intent = Intent(this, GenreActivity::class.java)
                startActivity(intent)
            } R.id.btnManKategori ->{
            var intent = Intent(this, KategoriActivity::class.java)
            startActivity(intent)
        }
        }
    }
}