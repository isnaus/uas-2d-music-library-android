package khasanah.isna.musiclibrary

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_kategori.*


class AdapterDataKategori(val dataKategori: List<HashMap<String,String>>,
                          val kategoriActivity: KategoriActivity
) : //new
    RecyclerView.Adapter<AdapterDataKategori.HolderDataKategori>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataKategori {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data_kategori,p0,false)
        return HolderDataKategori(v)
    }

    override fun getItemCount(): Int {
        return dataKategori.size
    }

    override fun onBindViewHolder(p0: HolderDataKategori, p1: Int) {
        val data = dataKategori.get(p1)
        p0.txIdKategori.setText(data.get("id_kategori"))
        p0.txNamaKategori.setText(data.get("nama_kategori"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            kategoriActivity.edIdKategori.setText(data.get("id_kategori"))
            kategoriActivity.edNamaKategori.setText(data.get("nama_kategori"))
        })
        //endNew
    }

    class HolderDataKategori(v: View) : RecyclerView.ViewHolder(v){
        val txIdKategori = v.findViewById<TextView>(R.id.txIdKategori)
        val txNamaKategori = v.findViewById<TextView>(R.id.txNamaKategori)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout) //new
    }
}