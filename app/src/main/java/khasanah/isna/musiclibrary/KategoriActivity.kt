package khasanah.isna.musiclibrary


import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import khasanah.isna.musiclibrary.AdapterDataKategori
import khasanah.isna.musiclibrary.R
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kategori.*
import org.json.JSONArray
import org.json.JSONObject


class KategoriActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var kategoriAdapter : AdapterDataKategori
    var daftarKategori = mutableListOf<HashMap<String,String>>()
    var url4 = "http://192.168.43.13/musiclibrary/show_data_kategori.php"
    var url5 = "http://192.168.43.13/musiclibrary/query_upd_del_ins_kategori.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kategori)
        kategoriAdapter = AdapterDataKategori(daftarKategori,this)
        lvKategori.layoutManager = LinearLayoutManager(this)
        lvKategori.adapter = kategoriAdapter
        btnInsertKategori.setOnClickListener(this)
        btnUpdateKategori.setOnClickListener(this)
        btnDeleteKategori.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataKategori()
    }

    fun queryInsertUpdateDeleteKategori(mode : String){
        val request = object : StringRequest(
            Method.POST,url5,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataKategori()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_kategori",edIdKategori.text.toString())
                        hm.put("nama_kategori",edNamaKategori.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_kategori",edIdKategori.text.toString())
                        hm.put("nama_kategori",edNamaKategori.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_kategori",edIdKategori.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataKategori(){
        val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kategori = HashMap<String,String>()
                    kategori.put("id_kategori",jsonObject.getString("id_kategori"))
                    kategori.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    daftarKategori.add(kategori)
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertKategori ->{
                queryInsertUpdateDeleteKategori("insert")
            }
            R.id.btnUpdateKategori ->{
                queryInsertUpdateDeleteKategori("update")
            }
            R.id.btnDeleteKategori ->{
                queryInsertUpdateDeleteKategori("delete")
            }
        }
    }

}