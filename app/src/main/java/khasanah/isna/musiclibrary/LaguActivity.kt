package khasanah.isna.musiclibrary

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_lagu.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class LaguActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var mediaHelper: MediaHelper
    lateinit var laguAdapter : AdapterDataLagu
    lateinit var kategoriAdapter : ArrayAdapter<String>
    lateinit var genreAdapter : ArrayAdapter<String>
    var daftarLagu = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    var daftarGenre = mutableListOf<String>()
    var url1 = "http://192.168.43.13/musiclibrary/show_data.php"
    var url2 = "http://192.168.43.13/musiclibrary/get_kategori.php"
    var url3 = "http://192.168.43.13/musiclibrary/get_genre.php"
    var url4 = "http://192.168.43.13/musiclibrary/query_upd_del_ins.php"
    var imStr = ""
    var pilihGenre = ""
    var pilihKategori = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lagu)
        laguAdapter = AdapterDataLagu(daftarLagu,this)
        mediaHelper = MediaHelper(this)
        lvLagu.layoutManager = LinearLayoutManager(this)
        lvLagu.adapter = laguAdapter

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarKategori)
        spKategori.adapter = kategoriAdapter
        spKategori.onItemSelectedListener = itemSelected

        genreAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarGenre)
        spGenre.adapter = genreAdapter
        spGenre.onItemSelectedListener = itemSelected1


        imUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataLagu()
        getKategori()
        getGenre()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }

    }
    val itemSelected1 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {

            spGenre.setSelection(0)
            pilihGenre = daftarGenre.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihGenre = daftarGenre.get(position)
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data!!,imUpload)
            }
        }
    }

    fun queryInsertUpdateDeleteLagu(mode : String){
        val request = object : StringRequest(
            Method.POST,url4,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataLagu()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_lagu",edIdLagu.text.toString())
                        hm.put("judul",edJudul.text.toString())
                        hm.put("artist",edPenyanyi.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kategori",pilihKategori)
                        hm.put("nama_genre",pilihGenre)
                        hm.put("tahun",edTahun.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_lagu",edIdLagu.text.toString())
                        hm.put("judul",edJudul.text.toString())
                        hm.put("artist",edPenyanyi.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kategori",pilihKategori)
                        hm.put("nama_genre",pilihGenre)
                        hm.put("tahun",edTahun.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_lagu",edIdLagu.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getKategori(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nama_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getGenre(){
        val request = StringRequest(
            Request.Method.POST,url3,
            Response.Listener { response ->
                daftarGenre.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarGenre.add(jsonObject.getString("nama_genre"))
                }
                genreAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataLagu(){
        val request = StringRequest(
            Request.Method.POST,url1,
            Response.Listener { response ->
                daftarLagu.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var lagu = HashMap<String,String>()
                    lagu.put("id_lagu",jsonObject.getString("id_lagu"))
                    lagu.put("judul",jsonObject.getString("judul"))
                    lagu.put("artist",jsonObject.getString("artist"))
                    lagu.put("tahun",jsonObject.getString("tahun"))
                    lagu.put("url",jsonObject.getString("url"))
                    lagu.put("nama_genre",jsonObject.getString("nama_genre"))
                    lagu.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    daftarLagu.add(lagu)
                }
                laguAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){ R.id.imUpload ->{
            val intent = Intent()
            intent.setType("image/*")
            intent.setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(intent,mediaHelper.getRcGallery())
        }
            R.id.btnInsert ->{
                queryInsertUpdateDeleteLagu("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDeleteLagu("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDeleteLagu("delete")
            }
        }
    }

}