package khasanah.isna.musiclibrary

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_music.*
import kotlinx.android.synthetic.main.row_data_music.*
import java.util.concurrent.TimeUnit

class MusicActivity : AppCompatActivity() , View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    //Inisiasi Variabel
    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var FONT_COLOR = "font_color"
    var FONT_SIZE = "font__size"

    val DEF_BG_COLOR = "WHITE"
    val DEF_FONT_COLOR  = "#171D1C"
    val DEF_FONT_SIZE = 24
    var FontColor: String = ""
    var BGTitleColor: String = ""
    val arrayBGTitleColor = arrayOf("BLUE", "YELLOW", "GREEN", "BLACK", "WHITE")

    var posLaguSkrg = 0
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: android.widget.MediaController

    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter

    //Seekbar listener
    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    //Button Listener
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                setSong(posLaguSkrg)
                audioPlay()
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)
        mediaController = android.widget.MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max = 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        asu.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        showDataLagu()
        loadSetting()
    }

    //additional function

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        posLaguSkrg=position
        audioStop()
        setSong(position)
    }

    fun showDataLagu(){
        val cursor : Cursor = db.query("lagu", arrayOf("id_lagu as _id","judul","artis","gambar"),null,null,null,null,"id_lagu asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_data_lagu,cursor,
            arrayOf("judul","artis","gambar"), intArrayOf( R.id.txJudulMusic, R.id.txArtist,R.id.imageView3),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        asu.adapter = adapter
//        var jmllagu = lsLagu.adapter.count
//        Log.d("output","Jumlah lagu : $jmllagu")
    }

    fun setSong(pos: Int){
        val c: Cursor = asu.adapter.getItem(pos) as Cursor
        var id_music = c.getInt(c.getColumnIndex("_id"))
        var judul = c.getString(c.getColumnIndex("judul"))
        var artis = c.getString(c.getColumnIndex("artis"))
        var gambar = c.getInt(c.getColumnIndex("gambar"))
//        Log.d("output","Posisi : $pos")
//        Log.d("output","ID music : $id_music")
//        Log.d("output","ID Cover : $id_cover")
//        Log.d("output","Judul Lagu : $judul")
        mediaPlayer = MediaPlayer.create(this, id_music)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress=mediaPlayer.currentPosition
        imV.setImageResource(gambar)
        txJudulLagu.setText(judul)
        txArtis.setText(artis)
    }

    fun milliSecondToString(ms : Int):String{
        var detik : Long = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit : Long = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(){
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg<(asu.adapter.count-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        setSong(posLaguSkrg)
        audioPlay()
    }

    fun audioPrev(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = asu.adapter.count-1
        }
        setSong(posLaguSkrg)
        audioPlay()
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime : Int = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this, 50)
        }
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val DetailLayout = findViewById(R.id.CLLayout) as ConstraintLayout
        CLLayout.setBackgroundColor(Color.parseColor(preferences.getString(BG_COLOR,DEF_BG_COLOR)))
        txArtis.setTextColor(Color.parseColor(preferences.getString(FONT_COLOR,DEF_FONT_COLOR)))
        txJudulLagu.setTextColor(Color.parseColor(preferences.getString(FONT_COLOR,DEF_FONT_COLOR)))
        txArtis.textSize = preferences.getInt(FONT_SIZE,DEF_FONT_SIZE).toFloat()
        txJudulLagu.textSize = preferences.getInt(FONT_SIZE,DEF_FONT_SIZE).toFloat()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,ActivitySetting::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}