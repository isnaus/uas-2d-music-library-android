package khasanah.isna.musiclibrary

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_lagu.*


class AdapterDataLagu(val dataLagu: List<HashMap<String,String>>,
                      val laguActivity: LaguActivity
) : //new
    RecyclerView.Adapter<AdapterDataLagu.HolderDataLagu>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataLagu {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data_lagu,p0,false)
        return HolderDataLagu(v)
    }

    override fun getItemCount(): Int {
        return dataLagu.size
    }

    override fun onBindViewHolder(p0: HolderDataLagu, p1: Int) {
        val data = dataLagu.get(p1)
        p0.txIdLagu.setText(data.get("id_lagu"))
        p0.txJudul.setText(data.get("judul"))
        p0.txArtist.setText(data.get("artist"))
        p0.txGenre.setText(data.get("nama_genre"))
        p0.txKategori.setText(data.get("nama_kategori"))
        p0.txTahun.setText(data.get("tahun"))


        //beginNew
        if(p1.rem(2) == 0) p0.aLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.aLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.aLayout.setOnClickListener(View.OnClickListener {
            val pos = laguActivity.daftarKategori.indexOf(data.get("nama_kategori"))
            laguActivity.spKategori.setSelection(pos)
            val pos1 = laguActivity.daftarGenre.indexOf(data.get("nama_genre"))
            laguActivity.spGenre.setSelection(pos1)
            laguActivity.edIdLagu.setText(data.get("id_lagu"))
            laguActivity.edJudul.setText(data.get("judul"))
            laguActivity.edPenyanyi.setText(data.get("artist"))
            laguActivity.edTahun.setText(data.get("tahun"))
            Picasso.get().load(data.get("url")).into(laguActivity.imUpload)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.gambar);
    }

    class HolderDataLagu(v: View) : RecyclerView.ViewHolder(v){
        val txIdLagu = v.findViewById<TextView>(R.id.txIdLagu)
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val txArtist = v.findViewById<TextView>(R.id.txArtist)
        val txGenre = v.findViewById<TextView>(R.id.txGenre)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val txTahun = v.findViewById<TextView>(R.id.txTahun)
        val gambar = v.findViewById<ImageView>(R.id.imageView)
        val aLayout = v.findViewById<ConstraintLayout>(R.id.aLayout) //new
    }
}