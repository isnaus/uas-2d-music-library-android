package khasanah.isna.musiclibrary
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.fonts.Font
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_music.*
import kotlinx.android.synthetic.main.activity_setting.*

class ActivitySetting : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    //inisiasi variabel
    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var FONT_COLOR = "font_color"
    var FONT_SIZE = "font__size"

    val DEF_BG_COLOR = "WHITE"
    val DEF_FONT_COLOR  = "BLACK"
    val DEF_FONT_SIZE = 24
    var FontColor: String = ""
    var BGTitleColor: String = ""
    val arrayBGTitleColor = arrayOf("BLUE", "YELLOW", "GREEN", "BLACK", "WHITE")
    lateinit var adapterSpin: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        //listener
        btnSimpan.setOnClickListener(this)
        rgBGHColor.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBlue -> FontColor = "BLUE"
                R.id.rbYellow -> FontColor = "YELLOW"
                R.id.rbGreen -> FontColor = "GREEN"
                R.id.rbBlack -> FontColor = "BLACK"
                R.id.rbWhite -> FontColor = "WHITE"
            }
        }
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayBGTitleColor)
        spBGTitleColor.adapter = adapterSpin
        spBGTitleColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGTitleColor = adapterSpin.getItem(position).toString()
            }
        }
        //get resources from SharedPreferences
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection =
            adapterSpin.getPosition(preferences.getString(BG_COLOR, DEF_BG_COLOR))
        spBGTitleColor.setSelection(spinnerselection, true)
        var rgselection = preferences.getString(FONT_COLOR, DEF_FONT_COLOR)
        if (rgselection == "BLUE") {
            rgBGHColor.check(R.id.rbBlue)
        } else if (rgselection == "YELLOW") {
            rgBGHColor.check(R.id.rbYellow)
        } else if (rgselection == "GREEN") {
            rgBGHColor.check(R.id.rbGreen)
        } else if (rgselection == "BLACK"){
            rgBGHColor.check(R.id.rbBlack)
        }else {
            rgBGHColor.check(R.id.rbWhite)
        }
        sbFTitle.progress = preferences.getInt(FONT_SIZE, DEF_FONT_SIZE)
        sbFTitle.setOnSeekBarChangeListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSimpan -> {
                //save configuration to SharedPreferences
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_COLOR, BGTitleColor)
                prefEditor.putString(FONT_COLOR, FontColor)
                prefEditor.putInt(FONT_SIZE, sbFTitle.progress)
                prefEditor.commit()
                Toast.makeText(this, "Setting Disimpan", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when (seekBar?.id) {
            R.id.sbFTitle -> {

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }


}