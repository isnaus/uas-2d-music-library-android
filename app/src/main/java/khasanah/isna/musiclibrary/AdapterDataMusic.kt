package khasanah.isna.musiclibrary

import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Handler
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_music.*

class AdapterDataMusic(val dataMusic: List<HashMap<String,String>>,
                       val musicActivity: MusicActivity
) : //new
    RecyclerView.Adapter<AdapterDataMusic.HolderDataMusic>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataMusic {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data_music,p0,false)
        return HolderDataMusic(v)
    }

    override fun getItemCount(): Int {
        return dataMusic.size
    }

    override fun onBindViewHolder(p0: HolderDataMusic, p1: Int) {
        val data = dataMusic.get(p1)
        p0.txIdLagu.setText(data.get("id_lagu"))
        p0.txJudul.setText(data.get("judul"))
        p0.txArtist.setText(data.get("artist"))
        p0.txMuxic.setText(data.get("url2"))


        //beginNew
        if(p1.rem(2) == 0) p0.dLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.dLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.dLayout.setOnClickListener(View.OnClickListener {
//            musicActivity.txJudulLagu.setText(data.get("id_lagu"))
            musicActivity.txJudulLagu.setText(data.get("judul"))
            musicActivity.txArtis.setText(data.get("artist"))
            Picasso.get().load(data.get("url")).into(musicActivity.imV)
//            musicActivity.mediaPlayer.setDataSource(data.get("url2"))
            musicActivity.mediaPlayer.setDataSource(data.get("url2"))
//            musicActivity.mediaPlayer.start()
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.gambar);
    }

    class HolderDataMusic(v: View) : RecyclerView.ViewHolder(v){
        val txIdLagu = v.findViewById<TextView>(R.id.txIdLagu)
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val txArtist = v.findViewById<TextView>(R.id.txArtist)
        val gambar = v.findViewById<ImageView>(R.id.imageView)
        val txMuxic = v.findViewById<TextView>(R.id.txMusic)
        val dLayout = v.findViewById<ConstraintLayout>(R.id.dLayout) //new
    }


}