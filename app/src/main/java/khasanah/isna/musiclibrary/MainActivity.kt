package khasanah.isna.musiclibrary

import android.content.Intent
import android.os.Bundle
import android.view.View

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLagu.setOnClickListener(this)
        btnMan.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLagu ->{
                var intent = Intent(this,MusicActivity::class.java)
                startActivity(intent)
            }
            R.id.btnMan ->{
                var intent = Intent(this, ManajementActivity::class.java)
                startActivity(intent)
            }
        }
    }

}